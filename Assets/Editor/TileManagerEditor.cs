﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(TileManager))]
public class TileManagerEditor : Editor {

	public override void OnInspectorGUI(){
		base.OnInspectorGUI();
		TileManager controller = (TileManager)target;
		if(GUILayout.Button("Reload", "box")){
			controller.Reload();
			SceneView.RepaintAll();
		}
	}

	void OnEnable(){
		SceneView.onSceneGUIDelegate += OnSceneViewEvent;
	}

	void OnDisable(){
		SceneView.onSceneGUIDelegate -= OnSceneViewEvent;
	}

	public void OnSceneViewEvent(SceneView sceneview){
		TileManager controller = (TileManager)target;
		Vector3 position = controller.transform.position;

		/* Force the gameobject to position itself x and y number divisible by 0.5 */
		position = new Vector3(Mathf.Ceil(position.x * 2)/2, Mathf.Ceil(position.y * 2)/2, position.z);
		controller.transform.position = position;

		Event e = Event.current;
		if(controller.isEditing){
			Ray ray = Camera.current.ScreenPointToRay(new Vector3(e.mousePosition.x, -e.mousePosition.y + Camera.current.pixelHeight));
			Vector3 mousePos = ray.origin;

			RaycastHit hit;
			if(Physics.Raycast(ray, out hit)){
				if(e.type == EventType.keyUp && e.keyCode == KeyCode.A){

//					GameObject obj = Instantiate(controller.modelObject) as GameObject;
//					obj.transform.parent = controller.transform;
					Vector3 pos = Vector3.zero;

					/*WORKING*/
					if(position.x % 1 == 0){
						if(controller.rows % 2 == 0){
							pos.x = Mathf.Floor(mousePos.x/controller.width) + controller.width/2;
						}
						else{
							pos.x = Mathf.Round(mousePos.x/controller.width);
						}
					}
					else{
						if(controller.rows % 2 == 0){
							pos.x = Mathf.Round(mousePos.x/controller.width);
						}
						else{
							pos.x = Mathf.Floor(mousePos.x/controller.width) + controller.width/2;
						}
					}

					if(position.y % 1 == 0){
						if(controller.cols % 2 == 0){
							pos.y = Mathf.Floor(mousePos.y/controller.height) + controller.height/2; 
						}
						else{
							pos.y = Mathf.Round(mousePos.y/controller.height);
						}
					}
					else{
						if(controller.cols % 2 == 0){
							pos.y = Mathf.Round(mousePos.y/controller.height);
						}
						else{
							pos.y = Mathf.Floor(mousePos.y/controller.height) + controller.height/2; 
						}	
					}
					controller.CreateTileInPosition(pos);
				}
				e.Use();
			}
		}
	}
}
