﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TileManager : MonoBehaviour {

	public GameObject modelObject;
	public bool isEditing = false;
	public float width =  1;
	public float height = 1;
	public int rows = 4;
	public int cols = 4;
	public List<Cell> cells =  new List<Cell>();
	public List<Transform> tiles = new List<Transform>();

	void OnDrawGizmos(){
		Gizmos.color = Color.red;
		Vector3 pos = transform.position;
		Vector3 size = transform.collider.bounds.size;
		//Draw horizontal lines
		for(int x=0; x<=cols; x++){
			Gizmos.DrawLine(new Vector3((pos.x-size.x/2) +x*width, pos.y-size.y/2, pos.z),
			                new Vector3((pos.x-size.x/2) +x*width, (pos.y-size.y/2) + rows*height, pos.z));
		}

		for(int y=0; y<=rows; y++){
			Gizmos.DrawLine(new Vector3((pos.x-size.x/2), (pos.y-size.y/2) +y*height, pos.z),
			                new Vector3((pos.x-size.x/2) + cols*width, (pos.y-size.y/2)  +y*height, pos.z));
		}

		BoxCollider collidr = GetComponent<BoxCollider>();
		collidr.size  = new Vector3(width * cols, height *rows, collidr.size.z);
		
		cells.Clear();
		for(int y=0; y<rows; y++){
			for(int x=0; x<cols; x++){
				Vector3 temp = Vector3.zero;
				temp.x = ((x*width) - collidr.size.x/2 + width/2);
				temp.y = ((y*height) - collidr.size.y/2 + height/2);
				
				Cell cell = cells.Find(item => item.name == (x+y*cols).ToString());
				if(cell == null){
					cell = new Cell();
					cell.name = (x+y*cols).ToString();
					cells.Add(cell);
				}
				cell.position = temp;
			}
		}
	}

	public void Reload(){
		RepositionTiles();
	}
	
	public void CreateTileInPosition(Vector3 position){
		GameObject obj = Instantiate(modelObject) as GameObject;
		obj.transform.parent = transform;

		//assign to object position to get the localposition later
		obj.transform.position = position;
		
		//get the cell with the same name
		Debug.Log(position);
		Cell cell = cells.Find(item => item.position == obj.transform.localPosition);
		if(cell.name != null){
			obj.name = cell.name;
			tiles.Add(obj.transform);
		}
	}

	void RepositionTiles(){

		for (int x=0; x<tiles.Count; x++){
			Transform t = tiles[x];
			if(t != null){
				Cell cell = cells.Find(item => item.name == t.name);
				if(cell == null){
					tiles.Remove(t);
					DestroyImmediate(t.gameObject);
				}
				else{
					tiles[x].localPosition = cell.position;
				}
			}
			else{
				tiles.Remove(tiles[x]);
			}
		}
	}
}

[System.Serializable]
public class Cell{
	public string name;
	public Vector3 position;
}
